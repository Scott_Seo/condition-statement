package com.fimm.test;

import com.fimm.conditions.ForCondition;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
public class JavaBeanForTest implements ForCondition {
    private String propertyString;
    private Integer propertyInteger;

    public String getPropertyString() {
        return propertyString;
    }

    public void setPropertyString(String propertyString) {
        this.propertyString = propertyString;
    }

    public Integer getPropertyInteger() {
        return propertyInteger;
    }

    public void setPropertyInteger(Integer propertyInteger) {
        this.propertyInteger = propertyInteger;
    }
}