package com.fimm;

import com.fimm.test.JavaBeanForTest;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.fimm.ConditionStatement.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
public class ConditionStatementTest {
    @Test
    // 조건 확인 대상이 Map 인 경우 entry 관련 구문을 사용할 수 있다.
    public void test2() throws Exception {
        //Given
        Map<String, Object> sourceMap = new HashMap<String, Object>() {
            {
                put("Name", "Seo");
                put("number", 100);
            }
        };

        //Expect
        assertThat(does(sourceMap, entry("Name", eq("Seo"))), is(true));
        assertThat(does(sourceMap, entry("number", eq(100))), is(true));

        assertThat(does(sourceMap, hasEntry("Name")), is(true));
        assertThat(does(sourceMap, hasEntry("number")), is(true));
    }

    @Test
    // 조건 확인 대상이 JavaBean 인 경우 property 관련 구문을 사용할 수 있다. 이 경우 property value는 모두 String 이다.
    public void test3() throws Exception {
        //Given
        JavaBeanForTest javabean = new JavaBeanForTest() {
            {
                setPropertyString("value");
                setPropertyInteger(100);
            }
        };

        //Expect
        assertThat(does(javabean, property("propertyString", eq("value"))), is(true));
        assertThat(does(javabean, property("propertyInteger", eq("100"))), is(true));
        assertThat(does(javabean, hasProperty("propertyString")), is(true));
        assertThat(does(javabean, hasProperty("propertyInteger")), is(true));
    }
}