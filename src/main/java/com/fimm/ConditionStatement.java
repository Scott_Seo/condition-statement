package com.fimm;

import com.fimm.conditions.*;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public interface ConditionStatement {
    String NO_COMPATIBLE_SOURCE = "지원하지 않습니다.";
    int FIRST_INDEX = 0;

    static boolean does(ForCondition conditionSource, Condition condition) {
        return condition.check(conditionSource);
    }

    static boolean does(Map sourceMap, MapCondition mapCondition) {
        return mapCondition.check(sourceMap);
    }

    static boolean does(List sourceList, ListCondition listCondition) {
        return listCondition.check(sourceList);
    }

    static boolean does(List sourceList, Condition condition) {
        return condition.check(sourceList);
    }

    static boolean does(String sourceString, StringCondition stringCondition) {
        return stringCondition.check(sourceString);
    }

    static boolean does(String sourceString, Condition condition) {
        return condition.check(sourceString);
    }

    /** ListCondition */
    static ListCondition firstData(Condition condition) {
        return sourceList -> condition.check(CollectionUtils.get(sourceList, FIRST_INDEX));
    }

    /** MapCondition */
    static MapCondition meet(MapCondition mapCondition) {
        return source -> mapCondition.test(source);
    }

    static MapCondition and(MapCondition... mapConditions) {
        return source -> Arrays.stream(mapConditions).allMatch(condition -> condition.test(source));
    }

    static MapCondition or(MapCondition... mapConditions) {
        return source -> Arrays.stream(mapConditions).anyMatch(condition -> condition.test(source));
    }

    static MapCondition hasEntry(String entryKey) {
        return source -> source.get(entryKey) != null;
    }

    static MapCondition entry(String entryKey, Condition condition) {
        return source -> condition.check(MapUtils.getObject(source, entryKey));
    }

    static MapCondition not(MapCondition condition) {
        return source -> !condition.check(source);
    }

    /** StringCondition */
    static StringCondition isContainedIn(List<String> list) {
        return source -> list.stream().anyMatch(compareString -> (StringUtils.equals((String) source, compareString)));
    }

    static StringCondition isNotContainedIn(List<String> list) {
        return source -> list.stream().anyMatch(compareString -> (StringUtils.equals(source, compareString))) == false;
    }

    static StringCondition isNotEmpty() {
        return source -> !StringUtils.isEmpty(source);
    }

    static StringCondition blank() {
        return source -> StringUtils.isBlank((String) source);
    }

    static StringCondition eq(String compareString) {
        return source -> StringUtils.equals(source, compareString);
    }

    /** Picker */
    static MapPicker entry(String entryKey) {
        return source -> MapUtils.getObject(source, entryKey);
    }

    static BeanPicker property(String propertyName) {
        return source -> {
            try {
                return BeanUtils.getProperty(source, propertyName);
            } catch (Exception e) {
                throw new ConditionStatementException(NO_COMPATIBLE_SOURCE, e);
            }
        };
    }

    /** ETC */
    static Condition meet(Condition condition) {
        return source -> condition.test(source);
    }

    static Condition eq(Object object) {
        return source -> source.equals(object);
    }

    static Condition isEmpty() {
        return source -> {
            if (source == null) {
                return true;
            } else if (source instanceof ForCondition) {
                return false;
            } else if (source instanceof List) {
                return CollectionUtils.isEmpty((List)source);
            } else if (source instanceof String) {
                return StringUtils.isEmpty((String)source);
            } else {
                throw new ConditionStatementException(NO_COMPATIBLE_SOURCE);
            }
        };
    }

    static Condition neq(Object object) {
        return source -> !source.equals(object);
    }

    static Condition and(Condition... conditions) {
        return source -> Arrays.stream(conditions).allMatch(condition -> condition.test(source));
    }

    static Condition or(Condition... conditions) {
        return source -> Arrays.stream(conditions).anyMatch(condition -> condition.test(source));
    }

    static Condition not(Condition condition) {
        return source -> !condition.check(source);
    }

    static Condition hasProperty(String propertyName) {
        return source -> {
            try {
                return (BeanUtils.getProperty(source, propertyName) != null);
            } catch (Exception e) {
                throw new ConditionStatementException(NO_COMPATIBLE_SOURCE, e);
            }
        };
    }

    static Condition isNull() {
        return source -> source == null;
    }

    static Condition isNotNull() {
        return source -> source != null;
    }

    static Condition isIn(Object... compareObjects) {
        return object -> Arrays.stream(compareObjects).anyMatch(source -> source.equals(object));
    }

    static Condition isNotIn(Object... compareObjects) {
        return object -> Arrays.stream(compareObjects).noneMatch(source -> source.equals(object));
    }

    static Condition all() {
        return source -> true;
    }

    static Condition property(String propertyName, Condition condition) {
        return source -> {
            try {
                return condition.check(BeanUtils.getProperty(source, propertyName));
            } catch (Exception e) {
                throw new ConditionStatementException(NO_COMPATIBLE_SOURCE, e);
            }
        };
    }
}
