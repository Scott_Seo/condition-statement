package com.fimm.conditions;

import java.util.Map;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
@FunctionalInterface
public interface MapCondition extends Condition {
	@SuppressWarnings("rawtypes")
	@Override
	default Class[] getCompatibleClasses() {
		return new Class[] { Map.class };
	}

	@Override
	default boolean test(Object source) {
		return test((Map) source);
	}

	boolean test(Map sourceMap);
}
