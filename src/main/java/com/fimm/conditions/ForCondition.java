package com.fimm.conditions;

/**
 * Condition-Statement 에서 사용될 객체임을 알리는 Marker interface
 *
 * @author fimm(seo.seokho_nhn.com)
 */
public interface ForCondition {
}
