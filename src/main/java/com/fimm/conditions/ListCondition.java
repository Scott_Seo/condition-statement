package com.fimm.conditions;

import java.util.List;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
@FunctionalInterface
public interface ListCondition extends Condition {
	@SuppressWarnings("rawtypes")
	@Override
	default Class[] getCompatibleClasses() {
		return new Class[] { List.class };
	}

	@Override
	default boolean test(Object source){
		return test((List) source);
	}

	boolean test(List sourceList);
}
