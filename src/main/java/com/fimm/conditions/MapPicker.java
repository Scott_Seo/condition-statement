package com.fimm.conditions;

import java.util.Map;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
@FunctionalInterface
public interface MapPicker {
    Object pick(Map sourceMap);
}
