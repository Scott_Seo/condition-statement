package com.fimm.conditions;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
@FunctionalInterface
public interface StringCondition extends Condition {
    @Override
    default Class[] getCompatibleClasses() {
        return new Class[]{String.class};
    }

    @Override
    default boolean test(Object source) {
        return test((String) source);
    }

    boolean test(String sourceString);
}
