package com.fimm.conditions;

/**
 * @author fimm(seo.seokho_nhn.com)
 */
@FunctionalInterface
public interface BeanPicker {
    Object pick(Object sourceBean);
}
